const mongoose = require('mongoose');
const Food = require('../../Models/foodsSchema.js');

module.exports.getActiveFoods = (request, response) => {

    Food.find({isActive: true})
    .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            return response.send(true)
        }
    })
    .catch(error => response.send(error));
    
}