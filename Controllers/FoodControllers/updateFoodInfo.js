const mongoose = require('mongoose');
const Food = require('../../Models/foodsSchema.js');
const auth = require('../../auth.js');

module.exports.updateFoodInfo = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    const input = request.body;

    const foodId = request.params.foodId;

    const updatedFoodInfo = {
        name: input.name,
        price: input.price,
        category: input.category
    };

    if (!userData.isAdmin) {
        return response.send(false);
    } else {
        Food.findById(foodId)
        .then(result => {
            if (result == null) {
                return response.send(false);
            } else {
                Food.findByIdAndUpdate(foodId, updatedFoodInfo, {new:true})
                .then(result => response.send(true))
                .catch(error => response.send(false))
            }
        })
        .catch(error => response.send(error));
    }
    
}