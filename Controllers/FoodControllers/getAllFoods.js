const mongoose = require('mongoose');
const Food = require('../../Models/foodsSchema.js');
const auth = require('../../auth.js');

module.exports.getAllFoods = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin) {
        return response.send(false);
    } else {
        Food.find({})
        .then(result => {
            if (result == null) {
                return response.send(false);
            } else {
                return response.send(true);
            }
        })
        .catch(error => response.send(error));
    }
    
}