const mongoose = require('mongoose');
const Food = require('../../Models/foodsSchema.js');

module.exports.getFood = (request, response) => {
    const foodId = request.params.foodId;

    Food.findById(foodId)
    .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            return response.send(true);
        }
    })
    .catch(error => response.send(error));
    
}