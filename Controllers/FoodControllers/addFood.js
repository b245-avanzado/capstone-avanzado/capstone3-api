const mongoose = require('mongoose');
const Food = require('../../Models/foodsSchema.js');
const auth = require('../../auth.js');

module.exports.addFood = (request, response) => {
    
    const userData = auth.decode(request.headers.authorization);

    let input = request.body;

    let newFood = new Food({
        name: input.name,
        price: input.price,
        category: input.category
    });

    Food.findOne({name: input.name})
    .then(result => {
        if (!userData.isAdmin) {
            return response.send(false);
        } else {
            if (result !== null) {
                return response.send(false);
            } else {
                newFood.save()
                return response.send(true);
            }
        }
    })
}