const mongoose = require('mongoose');
const Food = require('../../Models/foodsSchema.js');
const auth = require('../../auth.js');

module.exports.archiveFood = (request, response) => {

    const foodId = request.params.foodId;

    const userData = auth.decode(request.headers.authorization)

    if (!userData.isAdmin) {
        return response.send(false);
    } else {
        Food.findById(foodId)
        .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            if (!result.isActive) {
                return response.send(false);
            } else {
                Food.findByIdAndUpdate(foodId, {isActive:false}, {new:true})
                .then(result => response.send(true))
                .catch(error => response.send(false));
            }
        }
        })
        .catch(error => response.send(error));
    }

}