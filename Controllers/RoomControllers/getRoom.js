const mongoose = require('mongoose');
const Room = require('../../Models/roomsSchema.js');

module.exports.getRoom = (request, response) => {
    const roomId = request.params.roomId;

    Room.findById(roomId)
    .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            return response.send(result);
        }
    })
    .catch(error => response.send(error));
    
}