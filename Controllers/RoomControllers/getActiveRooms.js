const mongoose = require('mongoose');
const Room = require('../../Models/roomsSchema.js');

module.exports.getActiveRooms = (request, response) => {

    Room.find({isActive: true})
    .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            return response.send(result)
        }
    })
    .catch(error => response.send(error));
    
}