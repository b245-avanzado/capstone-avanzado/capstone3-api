const mongoose = require('mongoose');
const Room = require('../../Models/roomsSchema.js');
const auth = require('../../auth.js');

module.exports.addRoom = (request, response) => {
    
    const userData = auth.decode(request.headers.authorization);

    let input = request.body;

    let newRoom = new Room({
        name: input.name,
        description: input.description,
        price: input.price,
        availableRooms: input.availableRooms,
        picture: input.picture
    });

    Room.findOne({name: input.name})
    .then(result => {
        if (!userData.isAdmin) {
            return response.send(false);
        } else {
            if (result !== null) {
                return response.send(false);
            } else {
                newRoom.save()
                return response.send(true);
            }
        }
    })
}