const mongoose = require('mongoose');
const Room = require('../../Models/roomsSchema.js');
const auth = require('../../auth.js');

module.exports.archiveRoom = (request, response) => {

    const roomId = request.params.roomId;

    const userData = auth.decode(request.headers.authorization)

    if (!userData.isAdmin) {
        return response.send(false);
    } else {
        Room.findById(roomId)
        .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            if (!result.isActive) {
                return response.send(false);
            } else {
                Room.findByIdAndUpdate(roomId, {isActive:false}, {new:true})
                .then(result => response.send(true))
                .catch(error => response.send(false));
            }
        }
        })
        .catch(error => response.send(error));
    }

}