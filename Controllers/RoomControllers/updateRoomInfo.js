const mongoose = require('mongoose');
const Room = require('../../Models/roomsSchema.js');
const auth = require('../../auth.js');

module.exports.updateRoomInfo = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    const input = request.body;

    const roomId = request.params.roomId;

    const updatedRoomInfo = {
        name: input.name,
        description: input.description,
        price: input.price,
        availableRooms: input.availableRooms
    };

    if (!userData.isAdmin) {
        return response.send(false);
    } else {
        Room.findById(roomId)
        .then(result => {
            if (result == null) {
                return response.send(false);
            } else {
                Room.findByIdAndUpdate(roomId, updatedRoomInfo, {new:true})
                .then(result => response.send(result))
                .catch(error => response.send(false))
            }
        })
        .catch(error => response.send(error));
    }
    
}