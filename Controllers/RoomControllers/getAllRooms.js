const mongoose = require('mongoose');
const Room = require('../../Models/roomsSchema.js');
const auth = require('../../auth.js');

module.exports.getAllRooms = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin) {
        return response.send(false);
    } else {
        Room.find({})
        .then(result => {
            if (result == null) {
                return response.send(false);
            } else {
                return response.send(result);
            }
        })
        .catch(error => response.send(error));
    }
    
}