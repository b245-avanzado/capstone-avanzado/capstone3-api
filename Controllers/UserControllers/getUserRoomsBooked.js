const mongoose = require("mongoose");
const User = require("../../Models/usersSchema.js");
const auth = require("../../auth.js");

module.exports.getUserRoomsBooked = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id)
	.then(result => {
		if (result == null) {
			return response.send(false)
		} else {
			if (userData.isAdmin) {
				return response.send(false);
			} else {
				return response.send(result.roomsBooked);
			}
		}
	})
	.catch(error => response.send(error));
}