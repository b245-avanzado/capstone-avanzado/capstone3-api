const mongoose = require("mongoose");
const User = require("../../Models/usersSchema.js");
const bcrypt = require("bcrypt");

module.exports.adminRegistration = (request, response) => {
	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if (result !== null) {
			return response.send(false);
		} else {
			let newAdmin = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				mobileNo: input.mobileNo,
				password: bcrypt.hashSync(input.password, 10),
				isAdmin: true
			})

			newAdmin.save()
			.then(save => response.send(true))
			.catch(error => response.send(false));
		}
	})
	.catch(error => response.send(false));
}