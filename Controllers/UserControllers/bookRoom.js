const mongoose = require('mongoose');
const User = require('../../Models/usersSchema.js');
const Room = require('../../Models/roomsSchema.js');
const auth = require('../../auth.js')

module.exports.bookRoom = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    const roomId = request.params.roomId;

    const input = request.body;

    User.findById(userData._id)
    .then(user => {
        if (user == null) {
            return response.send(false);
        } else {
            if (userData.isAdmin) {
                return response.send(false);
            } else {
                Room.findById(roomId)
                .then(room => {
                    if (room == null) {
                        return response.send(false);
                    } else if (!room.isActive) {
                        return response.send(false);
                    } else {
                        user.roomsBooked.push({name: room.name, bookingDate: input.bookingDate});
                        room.clients.push({userId: userData._id, bookingDate: input.bookingDate});

                        user.save();
                        room.save();

                        return response.send(true);
                    }
                })
                .catch(error => response.send(false));
            }
        }
    })
    .catch(error => response.send(error));
    
}