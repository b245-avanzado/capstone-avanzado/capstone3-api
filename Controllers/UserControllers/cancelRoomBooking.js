const mongoose = require("mongoose");
const User = require("../../Models/usersSchema.js");
const auth = require("../../auth.js");

module.exports.cancelRoomBooking = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	const roomBookingId = request.params.roomBookingId;

	User.findById(userData._id)
	.then(user => {
		if (user == null) {
			return response.send(false)
		} else {
			if (userData.isAdmin) {
				return response.send(false);
			} else {

                let roomBookingIndex = user.roomsBooked.findIndex((roomBooked => roomBooked._id == roomBookingId))

                let bookingStatus = user.roomsBooked[roomBookingIndex].status;
                
				if (bookingStatus == "Cancelled") {
					return response.send(false);
				} else {
					user.roomsBooked[roomBookingIndex].status = "Cancelled";
                    user.save();
                    return response.send(true);
				}
				

			}
		}
	})
	.catch(error => response.send(error));
}