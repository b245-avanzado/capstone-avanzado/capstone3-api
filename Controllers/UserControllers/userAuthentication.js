const mongoose = require("mongoose");
const User = require("../../Models/usersSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../../auth.js");


module.exports.userAuthentication = (request, response) => {
	let input = request.body;
	
	User.findOne({email:input.email})
	.then(result => {
		if (result === null) {
			return response.send("Email is not yet registered. Register first before logging in!")
		} else {

			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password);

			if (isPasswordCorrect) {
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send("Password is incorrect!");
			}
		}
	})
	.catch(error => {
		return response.send(error);
	})
}