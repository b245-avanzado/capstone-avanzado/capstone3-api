const mongoose = require("mongoose");
const User = require("../../Models/usersSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../../auth.js");

module.exports.getUserProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id)
	.then(result => {
		if (result == null) {
			return response.send(false)
		} else {
			result.password = "********"
			return response.send(result);
		}
	})
	.catch(error => {
		return response.send(error);
	})

}