const mongoose = require('mongoose');
const User = require('../../Models/usersSchema.js');
const Cart = require('../../Models/cartSchema.js');
const auth = require('../../auth.js');

module.exports.getCart = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    User.findById(userData._id)
    .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            if(userData.isAdmin) {
                return response.send(false);
            } else {
                Cart.find({userId: userData._id})
                .then(cart => {
                    if (result == null) {
                        return response.send(false);
                    } else {
                        return response.send(true)
                    }
                })
                .catch(error => response.send(false));
            }
        }
    })
    .catch(error => response.send(error))
    
}