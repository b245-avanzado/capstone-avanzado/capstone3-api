const mongoose = require('mongoose');
const Activity = require('../../Models/activitiesSchema.js');

module.exports.getActivity = (request, response) => {
    const activityId = request.params.activityId;

    Activity.findById(activityId)
    .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            return response.send(true);
        }
    })
    .catch(error => response.send(error));
    
}