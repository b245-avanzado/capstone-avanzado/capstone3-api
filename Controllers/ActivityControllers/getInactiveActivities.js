const mongoose = require('mongoose');
const Activity = require('../../Models/activitiesSchema.js');
const auth = require('../../auth.js')

module.exports.getInactiveActivities = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if (!userData.isAdmin) {
        return response.send(false);
    } else {
        Activity.find({isActive: false})
        .then(result => {
            if (result == null) {
                return response.send(false);
            } else {
                return response.send(true)
            }
        })
        .catch(error => response.send(error));
    }
    
}