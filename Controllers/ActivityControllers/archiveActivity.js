const mongoose = require('mongoose');
const Activity = require('../../Models/activitiesSchema.js');
const auth = require('../../auth.js');

module.exports.archiveActivity = (request, response) => {

    const activityId = request.params.activityId;

    const userData = auth.decode(request.headers.authorization)

    if (!userData.isAdmin) {
        return response.send(false);
    } else {
        Activity.findById(activityId)
        .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            if (!result.isActive) {
                return response.send(false);
            } else {
                Activity.findByIdAndUpdate(activityId, {isActive:false}, {new:true})
                .then(result => response.send(true))
                .catch(error => response.send(false));
            }
        }
        })
        .catch(error => response.send(error));
    }

}