const mongoose = require('mongoose');
const Activity = require('../../Models/activitiesSchema.js');
const auth = require('../../auth.js');

module.exports.updateActivityInfo = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    const input = request.body;

    const activitySchema = request.params.activitySchema;

    const updatedActivityInfo = {
        name: input.name,
        price: input.price,
        category: input.category
    };

    if (!userData.isAdmin) {
        return response.send(false);
    } else {
        Activity.findById(activitySchema)
        .then(result => {
            if (result == null) {
                return response.send(false);
            } else {
                Activity.findByIdAndUpdate(activitySchema, updatedActivityInfo, {new:true})
                .then(result => response.send(true))
                .catch(error => response.send(false))
            }
        })
        .catch(error => response.send(error));
    }
    
}