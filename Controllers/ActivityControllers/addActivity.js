const mongoose = require('mongoose');
const Activity = require('../../Models/activitiesSchema.js');
const auth = require('../../auth.js');

module.exports.addActivity = (request, response) => {
    
    const userData = auth.decode(request.headers.authorization);

    let input = request.body;

    let newActivity = new Activity({
        name: input.name,
        description: input.description,
        price: input.price
    });

    Activity.findOne({name: input.name})
    .then(result => {
        if (!userData.isAdmin) {
            return response.send(false);
        } else {
            if (result !== null) {
                return response.send(false);
            } else {
                newActivity.save()
                return response.send(true);
            }
        }
    })
}