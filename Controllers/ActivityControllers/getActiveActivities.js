const mongoose = require('mongoose');
const Activity = require('../../Models/activitiesSchema.js');

module.exports.getActiveActivities = (request, response) => {

    Activity.find({isActive: true})
    .then(result => {
        if (result == null) {
            return response.send(false);
        } else {
            return response.send(true)
        }
    })
    .catch(error => response.send(error));
    
}