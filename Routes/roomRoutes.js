const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const addRoomController = require('../Controllers/RoomControllers/addRoom.js');
const archiveRoomController = require('../Controllers/RoomControllers/archiveRoom.js');
const getActiveRoomsController = require('../Controllers/RoomControllers/getActiveRooms.js');
const getAllRoomsController = require('../Controllers/RoomControllers/getAllRooms.js');
const getInactiveRoomsController = require('../Controllers/RoomControllers/getInactiveRooms.js');
const getRoomController = require('../Controllers/RoomControllers/getRoom.js');
const unarchiveRoomController = require('../Controllers/RoomControllers/unarchiveRoom.js');
const updateRoomInfoController = require('../Controllers/RoomControllers/updateRoomInfo.js');

// ROUTES WITHOUT PARAMS

router.post("/addRoom", auth.verify, addRoomController.addRoom);

router.get("/getActiveRooms", getActiveRoomsController.getActiveRooms);

router.get("/getAllRooms", auth.verify, getAllRoomsController.getAllRooms);

router.get("/getInactiveRooms", auth.verify, getInactiveRoomsController.getInactiveRooms);


// ROUTES WITH PARAMS

router.post("/archiveRoom/:roomId", auth.verify, archiveRoomController.archiveRoom);

router.get("/getRoom/:roomId", getRoomController.getRoom);

router.post("/unarchiveRoom/:roomId", auth.verify, unarchiveRoomController.unarchiveRoom);

router.post("/updateRoomInfo/:roomId", auth.verify, updateRoomInfoController.updateRoomInfo);


module.exports = router;