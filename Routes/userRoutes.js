const express = require('express');
const router = express.Router();
const auth = require("../auth.js");

const userRegistrationController = require('../Controllers/UserControllers/userRegistration.js');
const adminRegistrationController = require('../Controllers/UserControllers/adminRegistration.js');
const userAuthenticationController = require('../Controllers/UserControllers/userAuthentication.js');
const getUserProfileController = require('../Controllers/UserControllers/getUserProfile.js');
const bookRoomController = require('../Controllers/UserControllers/bookRoom.js');
const getUserRoomsBookedController = require('../Controllers/UserControllers/getUserRoomsBooked.js');
const cancelRoomBookingController = require('../Controllers/UserControllers/cancelRoomBooking.js');

// ROUTES WITHOUT PARAMS

router.post("/register", userRegistrationController.userRegistration);

router.post("/adminRegister", adminRegistrationController.adminRegistration);

router.post("/login", userAuthenticationController.userAuthentication);

router.get("/profile", auth.verify, getUserProfileController.getUserProfile);

router.get("/roomsBooked", auth.verify, getUserRoomsBookedController.getUserRoomsBooked);

// ROUTES WITH PARAMS

router.post("/bookRoom/:roomId", auth.verify, bookRoomController.bookRoom);

router.post("/cancelRoomBooking/:roomBookingId", auth.verify, cancelRoomBookingController.cancelRoomBooking);


module.exports = router;