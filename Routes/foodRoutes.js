const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const addFoodController = require('../Controllers/FoodControllers/addFood.js');
const archiveFoodController = require('../Controllers/FoodControllers/archiveFood.js');
const getActiveFoodsController = require('../Controllers/FoodControllers/getActiveFoods.js');
const getAllFoodsController = require('../Controllers/FoodControllers/getAllFoods.js');
const getInactiveFoodsController = require('../Controllers/FoodControllers/getInactiveFoods.js');
const getFoodController = require('../Controllers/FoodControllers/getFood.js');
const unarchiveFoodController = require('../Controllers/FoodControllers/unarchiveFood.js');
const updateFoodInfoController = require('../Controllers/FoodControllers/updateFoodInfo.js');

// ROUTES WITHOUT PARAMS

router.post("/addFood", auth.verify, addFoodController.addFood);

router.get("/getActiveFoods", getActiveFoodsController.getActiveFoods);

router.get("/getAllFoods", auth.verify, getAllFoodsController.getAllFoods);

router.get("/getInactiveFoods", auth.verify, getInactiveFoodsController.getInactiveFoods);


// ROUTES WITH PARAMS

router.post("/archiveFood/:foodId", auth.verify, archiveFoodController.archiveFood);

router.get("/getFood/:foodId", getFoodController.getFood);

router.post("/unarchiveFood/:foodId", auth.verify, unarchiveFoodController.unarchiveFood);

router.post("/updateFoodInfo/:foodId", auth.verify, updateFoodInfoController.updateFoodInfo);


module.exports = router;