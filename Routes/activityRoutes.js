const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const addActivityController = require('../Controllers/ActivityControllers/addActivity.js');
const archiveActivityController = require('../Controllers/ActivityControllers/archiveActivity.js');
const getActiveActivitiesController = require('../Controllers/ActivityControllers/getActiveActivities.js');
const getAllActivitiesController = require('../Controllers/ActivityControllers/getAllActivities.js');
const getInactiveActivitiesController = require('../Controllers/ActivityControllers/getInactiveActivities.js');
const getActivityController = require('../Controllers/ActivityControllers/getActivity.js');
const unarchiveActivityController = require('../Controllers/ActivityControllers/unarchiveActivity.js');
const updateActivityInfoController = require('../Controllers/ActivityControllers/updateActivityInfo.js');

// ROUTES WITHOUT PARAMS

router.post("/addActivity", auth.verify, addActivityController.addActivity);

router.get("/getActiveActivities", getActiveActivitiesController.getActiveActivities);

router.get("/getAllActivities", auth.verify, getAllActivitiesController.getAllActivities);

router.get("/getInactiveActivities", auth.verify, getInactiveActivitiesController.getInactiveActivities);


// ROUTES WITH PARAMS

router.post("/archiveActivity/:activityId", auth.verify, archiveActivityController.archiveActivity);

router.get("/getActivity/:activityId", getActivityController.getActivity);

router.post("/unarchiveActivity/:activityId", auth.verify, unarchiveActivityController.unarchiveActivity);

router.post("/updateActivityInfo/:activityId", auth.verify, updateActivityInfoController.updateActivityInfo);


module.exports = router;