const mongoose = require('mongoose');

const foodsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Food name is required!"]
    },
    price: {
        type: Number,
        required: [true, "Food price is required!"]
    },
    category: {
        type: String,
        required: [true, "Food category is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    clients: [
        {
            userId: {
                type: String,
                required: [true, "User Id is required."]
            },
            bookingDate: {
                type: Date,
                required: [true, 'Booking date is required.']
            },
            quantity: {
                type: Number,
                required: [true, "Please enter date of schedule."]
            }
        }
    ]
})

module.exports = mongoose.model("Food", foodsSchema);