const mongoose = require('mongoose');

const activitiesSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Activity name is required!"]
    },
    description: {
        type: String,
        require: [true, "Activity description is required!"]
    },
    price: {
        type: Number,
        required: [true, "Activity price is required!"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    onPromo: {
        type: Boolean,
        default: false
    },
    clients: [
        {
            userId: {
                type: String,
                required: [true, "User Id is required."]
            },
            bookingDate: {
                type: Date,
                required: [true, "Please enter date of schedule."]
            }
        }
    ]
})

module.exports = mongoose.model("Activity", activitiesSchema);