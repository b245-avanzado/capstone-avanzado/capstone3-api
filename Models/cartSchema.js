const mongoose = require('mongoose');

const cartsSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User Id is required!"]
    },
    itemId: {
        type: String,
        required: [true, "Item Id is required!"]
    },
    price: {
        type: Number,
        required: [true, "Price is required!"]
    },
    quantity: {
        type: Number,
        default: 1
    },
    subTotal: {
        $multiply: ['$price', '$quantity']
    }
})

module.exports = mongoose.model("Cart", cartsSchema);