const mongoose = require('mongoose');

const roomsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Room name is required!"]
    },
    description: {
        type: String,
        require: [true, "Room description is required!"]
    },
    price: {
        type: Number,
        required: [true, "Room price is required!"]
    },
    picture: {
        type: String,
        required: [true, "Picture is required!"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    onPromo: {
        type: Boolean,
        default: false
    },
    availableRooms: {
        type: Number,
        required: [true, "Number of available rooms is required!"]
    },
    clients: [
        {
            userId: {
                type: String,
                required: [true, "User Id is required."]
            },
            bookingDate: {
                type: Date,
                required: [true, "Please enter date of schedule."]
            }
        }
    ]
})

module.exports = mongoose.model("Room", roomsSchema);