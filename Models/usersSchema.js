const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema ({
	firstName: {
		type: String,
		default: "First Name"
	},
	lastName: {
		type: String,
		default: "Last Name"
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	mobileNo: {
		type: String,
		default: ""
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	roomsBooked: [
		{
			name: {
				type: String,
				required: [true, 'Room name is required!']
			},
			bookingDate: {
				type: Date,
				required: [true, 'Booking date is required!']
			},
			status: {
				type: String,
				default: 'Pending'
			}
		}
	],
	activitiesBooked: [
		{
			name: {
				type: String,
				required: [true, 'Activity name is required!']
			},
			bookingDate: {
				type: Date,
				required: [true, 'Booking date is required!']
			},
			status: {
				type: String,
				default: 'Pending'
			}
		}
	],
	foods: [
		{
			name: {
				type: String,
				required: [true, 'Food name is required!']
			},
			bookingDate: {
				type: Date,
				required: [true, 'Booking date is required!']
			},
			quantity: {
				type: Number,
				default: 1
			},
			status: {
				type: String,
				default: 'Pending'
			}
		}
	]

})

module.exports = mongoose.model('User', usersSchema);