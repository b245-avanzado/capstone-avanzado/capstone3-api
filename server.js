const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./Routes/userRoutes.js");
const roomRoutes = require("./Routes/roomRoutes.js");
const foodRoutes = require("./Routes/foodRoutes.js");
const activityRoutes = require("./Routes/activityRoutes.js");

const port = 4000;

const app = express();

mongoose.set('strictQuery', true);
// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@batch245-avanzado.ixvkr4o.mongodb.net/Resort_Booking-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

// For error handling and validation of connection
db.on("error", console.error.bind(console, "Connection Error"))
db.once("open", () => {console.log("We are now connected to the cloud")})

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(cors());

// Routing
app.use('/user', userRoutes);
app.use('/room', roomRoutes);
app.use('/food', foodRoutes);
app.use('/activity', activityRoutes);




app.listen(port, () => console.log(`Server is running at port ${port}!`))